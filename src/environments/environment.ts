// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCQXiqAgo-Ge4Nwf7NJrcfauFku7H0D008",
    authDomain: "project-akhir-tekweb.firebaseapp.com",
    projectId: "project-akhir-tekweb",
    storageBucket: "project-akhir-tekweb.appspot.com",
    messagingSenderId: "714698641226",
    appId: "1:714698641226:web:227b4e019d0ef54e237bb3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
