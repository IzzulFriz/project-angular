import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdoptDetailComponent } from './adopt-detail/adopt-detail.component';
import { HomeComponent } from './home/home.component';
import { InformationComponent } from './information/information.component';
import { AdminComponent } from './admin/admin.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialDesign } from '../material/material.module';
import { FormsModule } from '@angular/forms';
import { AdoptComponent } from './adopt/adopt.component';

const routes: Routes = [
  {
    path:'',
    component:AdminComponent,
    children:[
      {
        path:'home',
        component:HomeComponent
      },
      {
        path:'adopt',
        component:AdoptComponent
      },
      {
        path:'information',
        component:InformationComponent
      },
      {
        path:'',
        pathMatch:'full',
        redirectTo:'/admin/home' 
      }
    ]
  }
]

@NgModule({
  declarations: [
    AdminComponent, 
    AdoptDetailComponent,
    HomeComponent,
    InformationComponent,
    AdoptComponent
    
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialDesign,
    FormsModule
  ]
}
)
export class AdminModule { }