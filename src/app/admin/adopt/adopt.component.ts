import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';
import { AdoptDetailComponent } from '../adopt-detail/adopt-detail.component';

@Component({
  selector: 'app-adopt',
  templateUrl: './adopt.component.html',
  styleUrls: ['./adopt.component.scss']
})
export class AdoptComponent implements OnInit {
  title:any;
  cats:any={};
  userData:any = {};
  constructor(
    public dialog:MatDialog,
    public auth:AngularFireAuth,
    public db :AngularFirestore
  ) {

   }

  ngOnInit(): void {
    this.title='Adoption';
    this.auth.user.subscribe(user=>{
      this.userData = user;
      this.getCats();
    })
  }

  loading:boolean | undefined;
  getCats()
  {
    this.loading=true;
    this.db.collection('cats',ref=>{
      return ref.where('uid','==',this.userData.uid);
    }).valueChanges({idField : 'id'}).subscribe(result=>{
      console.log(result);
      this.cats=result;
      this.loading=false;
    },error=>{
      this.loading=false;
    });
  }


    AdoptDetail(data: any,idx: number)
    {
      let dialog= this.dialog.open(AdoptDetailComponent, {
          width: '400px',
          data: data,
      });
        dialog.afterClosed().subscribe(result=> {
        return;
        });
      }

      loadingDelete:any={};
      DeleteAdopt(id: any,idx: any)
      {
        var conf=confirm('Delete item?');
        if(conf)
        {
          this.db.collection('cats').doc(id).delete().then(result=>{
            this.cats.splice(idx,1);
            this.loadingDelete[idx]=false;
          }).catch(error=>{
            this.loadingDelete[idx]=false;
            alert('Tidak dapat menghapus data');
          });
        }
      }
    }
